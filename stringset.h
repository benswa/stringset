#ifndef __STRINGSET__
#define __STRINGSET__

// Insert a new string into the hash set and return a pointer to
// the string just inserted. If it is already there, nothing is
// inserted, and the previously-inserted string is returned.
const std::string* intern_stringset (const char*);

// Dumps out the string set in debug format.
void dump_stringset (std::ostream&);

#endif

