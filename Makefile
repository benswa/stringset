GPP   = g++ -g -O0 -Wall -Wextra -std=gnu++11
GRIND = valgrind --leak-check=full --show-reachable=yes

all : oc

oc : main.o stringset.o auxlib.o
	${GPP} main.o stringset.o auxlib.o -o oc

%.o : %.cpp
	${GPP} -c $<

ci :
	git add Makefile auxlib.h auxlib.cpp stringset.h stringset.cpp main.cpp README

spotless : clean
	- rm oc Listing.ps Listing.pdf test?.out test?.err

clean :
	-rm stringset.o main.o auxlib.o

# Depencencies.
main.o: main.cpp stringset.h
stringset.o: stringset.cpp stringset.h
auxlib.o: auxlib.cpp auxlib.h
