/*
   Name: Benson Liu
   ID: W1236292
   Course: CMPS 104a
   Assignment: 1
*/

// From Standard Library
#include <cstring> // C style string
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <libgen.h>
using namespace std;

// Additional Headers
#include "stringset.h"
#include "auxlib.h"

// Variables
string CPP = "/usr/bin/cpp";
const size_t LINESIZE = 1024;

// Chomp the last character from a buffer if it is delim.
void chomp (char* string, char delim) {
   size_t len = strlen (string);
   if (len == 0) return;
   char* nlpos = string + len - 1;
   if (*nlpos == delim) *nlpos = '\0';
}

// Run cpp against the lines of the file.
void cpplines (FILE* pipe, char* filename) {
   int linenr = 1;
   char inputname[LINESIZE];
   strcpy (inputname, filename);

   for (;;) {
      char buffer[LINESIZE];
      char* fgets_rc = fgets (buffer, LINESIZE, pipe);
      if (fgets_rc == NULL) break;

      chomp (buffer, '\n');

      // Scanning for preprocessor directives.
      // http://gcc.gnu.org/onlinedocs/cpp/Preprocessor-Output.html
      int sscanf_rc = sscanf (buffer, "# %d \"%[^\"]\"",
                              &linenr, filename);
      if (sscanf_rc == 2) {
         ////printf ("DIRECTIVE: line %d file \"%s\"\n", linenr, filename);
         continue;
      }

      char* savepos = NULL;   // For continuing strtok_r where last left off
      char* bufptr = buffer;
      for (int tokenct = 1;; ++tokenct) {
         char* token = strtok_r (bufptr, " \t\n", &savepos);
         bufptr = NULL;   // After first call, strtok_r require bufptr be NULL
         
         if (token == NULL) break;

         // Return value of intern_stringset() not used
         intern_stringset (token);
         //const string* str = intern_stringset (token);
         //cout << str << " -> \"" << *str << "\"" << endl;
      }
      ++linenr;
   }
}

int main (int argc, char** argv) {
   // Discovering debug flags
   int opt;
   while ((opt = getopt(argc, argv, "lyD:@:")) != -1) {
      switch (opt){
         case 'l':
            // Calling yylex()
            break;
         case 'y':
            // Calling yyparse()
            break;
         case 'D':
            // Pass this option and its argument to cpp. This is mostly useful
            // as -D__OCLIB_OH__ to suppress inclusion of the code from oclib.oh
            // when testing a program.
            // Pretty much extends the options to pass into the cpp program.
            CPP += " -D " + string (optarg);
            break;
         case '@':
            // Call set_debugflags, and use DEBUGF and DEBUGSTMT for debugging.
            // optarg is an external variable for this getopt function for @:
            set_debugflags (optarg);
            break;
      }
   }
   
   // Sets the execname to be this program
   set_execname (argv[0]);
   
   // According to specification, source file must be last parameter
   // Verifies the input file has the extension '.oc' and gets
   // the source file's name
   char* input_file = argv[argc - 1];
   string source_file_name = basename (input_file);
   size_t extension_pos = source_file_name.find_last_of(".");

   if (extension_pos == string::npos) {
      // If there isn't an input file or the file does not have an extension.
      errprintf ("Usage: oc [-ly] [-@ flag . . .] [-D string] program.oc\n");
      return get_exitstatus();
   }
   string file_extension = source_file_name.substr(extension_pos);
   if (file_extension != ".oc") {
      // If the source file does not have an .oc extension.
      errprintf ("Source file does not have the extension .oc\n");
      errprintf ("Usage: oc [-ly] [-@ flag . . .] [-D string] program.oc\n");
      return get_exitstatus();
   }

   // Retrieves the file name of the source file.
   string file_name = source_file_name.substr(0, extension_pos);

   // Sends the source file to the pre-processor
   string command = CPP + " " + input_file;
   FILE* pipe = popen (command.c_str(), "r");

   // Processes the contents of pipe
   if (pipe == NULL) {
      // If the pipe is NULL then print an error.
      syserrprintf (command.c_str());
   }
   else {
      // Processes the lines if the pipe returns something and
      // print the status when closing the pipe.
      cpplines (pipe, input_file);
      int pclose_rc = pclose (pipe);
      eprint_status (command.c_str(), pclose_rc);

      // Opening/Creating a file with the name <source_file_name>.str
      // Used as a guide:
      // http://www.cplusplus.com/reference/ostream/ostream/ostream/
      string output_file = source_file_name + ".str";
      filebuf fb;
      fb.open(output_file, ios::out);
      ostream file_stream(&fb);
      dump_stringset(file_stream);
      fb.close();
   }
   return get_exitstatus();
}
